ruby-otr-activerecord (2.5.0-1) unstable; urgency=medium

  * New upstream version 2.5.0.
  * Drop d/patches.
  * B-D: ruby-activerecord < 8.1.

 -- Utkarsh Gupta <utkarsh@debian.org>  Sat, 08 Mar 2025 06:39:23 +0530

ruby-otr-activerecord (2.0.3-3) unstable; urgency=medium

  * Team upload.
  * d/control (Build-Depends): Allow activerecord <6.1.
  * d/patches//patches/Fix-tests-for-AR-6.0.patch: Add patch.
    - Fix tests for AR 6.0.
  * d/patches/series: Enable new patch.

 -- Daniel Leidert <dleidert@debian.org>  Thu, 02 Dec 2021 19:00:23 +0100

ruby-otr-activerecord (2.0.3-2) unstable; urgency=medium

  * Team upload.
  * d/control (Build-Depends): Require activerecord >= 6.1 due to
    jhollinger/otr-activerecord/#38. This should fix autopkgtest in Testing.

 -- Daniel Leidert <dleidert@debian.org>  Tue, 30 Nov 2021 23:17:33 +0100

ruby-otr-activerecord (2.0.3-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
    - Adss support for Rails 6.1.

  [ Debian Janitor ]
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on ruby-activerecord and
      ruby-hashie-forbidden-attributes.
    + ruby-otr-activerecord: Drop versioned constraint on ruby-activerecord and
      ruby-hashie-forbidden-attributes in Depends.

  [ Daniel Leidert ]
  * d/control (Build-Depends): Raise debhelper to version 13. Allow rails <7.1.
    Add ruby-rspec and ruby-bundler for tests.
    (Standards-Version): Bump to 4.6.0.
    (Depends): Use ${ruby:Depends}.
  * d/copyright (Copyright): Add team.
  * d/ruby-otr-activerecord.examples: Install examples.
  * d/ruby-tests.rake: Add and enable tests.
  * d/rules: Install upstream changelog.
  * d/watch: Use github sources.
  * d/patches/Require-bundler-for-tests.patch: Add patch.
    - Require 'bundler' for Bundler namespace.
  * d/patches/series: Enable new patch.
  * d/upstream/metadata: Update Changelog URL.

 -- Daniel Leidert <dleidert@debian.org>  Tue, 30 Nov 2021 05:53:30 +0100

ruby-otr-activerecord (1.4.1-3) unstable; urgency=medium

  * Team upload.
  * d/control (Depends): Use specific dependencies instead of ruby:Depends to
    fix the dependency on ruby-activerecord using an epoch.

 -- Daniel Leidert <dleidert@debian.org>  Sun, 09 Feb 2020 22:29:06 +0100

ruby-otr-activerecord (1.4.1-2) unstable; urgency=medium

  * Source-only upload

 -- Utkarsh Gupta <utkarsh@debian.org>  Thu, 06 Feb 2020 09:38:38 -0500

ruby-otr-activerecord (1.4.1-1) unstable; urgency=medium

  * Initial release (Closes: #950651)

 -- Utkarsh Gupta <utkarsh@debian.org>  Tue, 04 Feb 2020 08:20:40 -0500
